import renderer from "react-test-renderer";
import React from "react";
//import ReactDOM from "react-dom";
import { shallow } from "enzyme";
import Home from "../pages/index";

// // React Test

// it("Site renders without crashing", () => {
//   const div = document.createElement("div");
//   ReactDOM.render(<App />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });

// describe("Addition", () => {
//   it("knows that 2 and 2 make 4", () => {
//     expect(2 + 2).toBe(4);
//   });
// });

// Enzyme Test

// -> https://pusher.com/tutorials/react-jest-enzyme

describe("App component", () => {
  // it("starts with a count of 0", () => {
  //   const wrapper = shallow(<App />);
  //   const text = wrapper.find("p").text();
  //   expect(text).toEqual("Count: 0");
  // });
  // it("increments count by 1 when the increment button is clicked", () => {
  //   const wrapper = shallow(<App />);
  //   const incrementBtn = wrapper.find("button.increment");
  //   incrementBtn.simulate("click");
  //   const text = wrapper.find("p").text();
  //   expect(text).toEqual("Count: 1");
  // });
  // it("decrements count by 1 when the decrement button is clicked", () => {
  //   const wrapper = shallow(<App />);
  //   const decrementBtn = wrapper.find("button.decrement");
  //   decrementBtn.simulate("click");
  //   const text = wrapper.find("p").text();
  //   expect(text).toEqual("Count: -1");
  // });

  it("Renders without crashing", () => {
    const wrapper = shallow(<Home />);
    const text = wrapper.find("h1").text();
    expect(text).toEqual("TV Shows");
  });

  it("matches the snapshot", () => {
    const tree = renderer.create(<Home />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
