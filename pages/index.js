//import Head, { Router } from "next/router";
import Head from "next/head";
import Link from "next/link";
import styles from "../styles/Home.module.css";

import React, { Component } from "react";
import cookies from "nookies";

const Home = ({ instance }) => {
  var currentInstance = "default instance";
  if (instance) {
    currentInstance = instance;
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>TV Shows</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>TV Shows</h1>
        <p className={styles.description}>
          <Link href="/us" as="/us">
            <a>What's on TV right now?</a>
          </Link>
        </p>
        <Link href="/us" as="/us">
          <a>
            <img src="/tvshows.png" alt="TV Shows" width="300px" />
          </a>
        </Link>
        <p>(Running on {currentInstance})</p>
      </main>
    </div>
  );
};

// class App extends Component {
//   constructor() {
//     super();
//     this.state = {
//       count: 0,
//     };
//   }

//   makeIncrementer = (amount) => () =>
//     this.setState((prevState) => ({
//       count: prevState.count + amount,
//     }));

//   increment = this.makeIncrementer(1);

//   decrement = this.makeIncrementer(-1);

//   render() {
//     return (
//       <div>
//         <p>Count: {this.state.count}</p>
//         <button className="increment" onClick={this.increment}>
//           Increment
//         </button>
//         <button className="decrement" onClick={this.decrement}>
//           Decrement
//         </button>
//       </div>
//     );
//   }
// }

Home.getInitialProps = async (context) => {
  console.log("browser: ", process.browser);

  //const myAppCookies = cookies.get(context);
  const { defaultCountry } = cookies.get(context);
  //console.log("TCL: myAppCookies", myAppCookies);

  const country = context.query.country || defaultCountry || "us";
  process.browser
    ? Router.replace("/[country]", `${country}`)
    : context.res.writeHead(302, { Location: `/${country}` });
  context.res.end();
  return { instance: process.env.INSTANCE }; // TODO: page reload
};

export default Home;
//export default App;
