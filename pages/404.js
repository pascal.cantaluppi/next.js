var parse = require("html-react-parser");

const NotFoundError = ({ statusCode }) => {
  return <h1>404 - Page Not Found</h1>;
};

export default NotFoundError;
