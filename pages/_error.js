var parse = require("html-react-parser");

const CustomError = ({ statusCode }) => {
  var errorMessage = "<b>Error:</b><br />";
  switch (statusCode) {
    case 404:
      errorMessage += "Resource not found..";
      break;
    case 500:
      errorMessage += "Internal server error..";
      break;
    default:
      errorMessage += "Oops! Something went wrong..";
  }
  return <div>{parse(errorMessage)}</div>;
};

CustomError.getInitialProps = (err, res) => {
  return { statusCode: res ? res.statusCode : err ? err.statusCode : 404 };
};

export default CustomError;
