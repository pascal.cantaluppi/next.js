import Head from "next/head";
import Error from "next/error";
import styles from "../../styles/Home.module.css";
import DetailStyles from "../../styles/DetailStyles.js";
import Thumbnail from "../../components/Thumbnail";
import axios from "axios";
import cookies from "nookies";

const Country = ({ shows = {}, country, statusCode }) => {
  if (statusCode) {
    return <Error statusCode={statusCode} />;
  }

  const renderShows = () => {
    if (!statusCode) {
      return shows.map((showItem, index) => {
        const { show } = showItem;
        return (
          <li key={index} className="tvshows__list">
            <Thumbnail
              imageUrl={(show.image && show.image.medium) || undefined}
              caption={show.name}
              href="/[country]/[showId]"
              as={`/${country}/${show.id}`}
            />
          </li>
        );
      });
    }
  };

  return (
    <div>
      <Head>
        <title>TV Shows</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.container}>
        <main className={styles.main}>
          <h1>TV Shows</h1>
          <ul className="shows__list" sytle={{ listStyleType: "none" }}>
            {renderShows()}
          </ul>
          <style jsx>{DetailStyles}</style>
        </main>
      </div>
    </div>
  );
};

// This function gets called at build time on server-side.
Country.getInitialProps = async (context) => {
  try {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0"); // January is 0!
    var yyyy = today.getFullYear();
    today = yyyy + "-" + mm + "-" + dd;

    const { defaultCountry } = cookies.get(context);
    const country = context.query.country || defaultCountry || "us";

    const res = await axios.get(
      `https://api.tvmaze.com/schedule?country=${country}&date=${today}`
    );
    return { shows: res.data, country };
  } catch (error) {
    console.error(error);
    return {
      statusCode: error.response ? error.response.status : 500,
    };
  }
};

export default Country;
