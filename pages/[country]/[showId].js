import Head from "next/head";
import Error from "next/error";
//import CustomError from "../_error";
import styles from "../../styles/Home.module.css";
import DetailStyles from "../../styles/DetailStyles.js";
import Cast from "../../components/Cast";
import axios from "axios";

var parse = require("html-react-parser");

const Details = ({ show = {}, statusCode }) => {
  const { name, image, summary, _embedded } = show;

  if (statusCode) {
    return (
      <Error
        statusCode={statusCode}
        //title="There was an error but we don't know why.."
      />
      //<CustomError />
    );
  }

  var imageUrl =
    (image && image.original) ||
    "https://via.placeholder.com/210x295?text=no%20image";

  return (
    <div>
      <Head>
        <title>TV Shows</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.container}>
        <main className={styles.main}>
          <h1>{name}</h1>
          <div
            className="details__poster"
            style={{
              backgroundImage: `url(${imageUrl})`,
            }}
          />
          <div className="details__container">{parse(summary)}</div>
          {_embedded.cast.length > 0 && <Cast cast={_embedded.cast} />}
          <style jsx>{DetailStyles}</style>
        </main>
      </div>
    </div>
  );
};

// This function gets called at build time on server-side.
Details.getInitialProps = async ({ query }) => {
  try {
    const { showId } = query;
    const res = await axios.get(
      `https://api.tvmaze.com/shows/${showId}?embed=cast`
    );
    return { show: res.data };
  } catch (error) {
    console.error(error);
    return {
      statusCode: error.response ? error.response.status : 500,
    };
  }
};

export default Details;
