import "bootstrap/dist/css/bootstrap.min.css";
//import styles from "../styles/Home.module.css";
import "../styles/globals.css";
import AppStyles from "../styles/AppStyles";
import Header from "../components/Header";

function MyApp({ Component, pageProps }) {
  return (
    <React.Fragment>
      <Header />
      <Component {...pageProps} />
      <style jsx>{AppStyles}</style>
    </React.Fragment>
  );
}

export default MyApp;
