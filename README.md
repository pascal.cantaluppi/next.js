# Learning Next.js

<p>
<img src="https://gitlab.com/pascal.cantaluppi/next.js/-/raw/master/public/nextjs.png" alt="Next.js" />
</p>

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

## The App

This App gives you information about the currently broadcast tv shows.

<p>
<img src="https://gitlab.com/pascal.cantaluppi/next.js/-/raw/master/public/preview01.png" />
<img src="https://gitlab.com/pascal.cantaluppi/next.js/-/raw/master/public/preview02.png" />
</p>

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
cls
