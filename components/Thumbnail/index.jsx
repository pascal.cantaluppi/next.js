import ThumbnailStyles from "./ThumbnailStyles.js";
import Link from "next/link";

const Thumbnail = ({
  imageUrl = "https://via.placeholder.com/210x295?text=no%20image",
  caption,
  href = "",
  as = "",
  small = false,
}) => {
  var imageStyle = "thumbnail__image";
  if (small) {
    imageStyle = imageStyle + "__small";
  }

  const hyperlink = () => {
    if (small) {
      return (
        <div>
          <img src={imageUrl} className={imageStyle} />
          <h4 className="thumbnail__caption">{caption}</h4>
        </div>
      );
    } else {
      return (
        <div>
          <Link href={href} as={as}>
            <a>
              <img src={imageUrl} className={imageStyle} />
              <h4 className="thumbnail__caption">{caption}</h4>
            </a>
          </Link>
        </div>
      );
    }
  };

  return (
    <div className="thumbnail">
      {hyperlink()}
      <style jsx>{ThumbnailStyles}</style>
    </div>
  );
};

export default Thumbnail;
