import css from "styled-jsx/css";

const ThumbnailStyles = css`
  .thumbnail__image {
    width: 100%;
  }
  .thumbnail__image__small {
    width: 200px;
  }
  .thumbnail__caption {
    text-align: center;
    padding: 10px;
  }
`;

export default ThumbnailStyles;
