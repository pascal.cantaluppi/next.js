import css from "styled-jsx/css";

const CastStyles = css`
  .cast {
    justify-content: left;
    align-items: left;
    text-align: center;
    width: 100%;
  }
  .cast__list {
    display: flex;
    overflow-x: auto;
  }
  .cast__list > :global(li) {
    margin-right: 10px;
  }
`;

export default CastStyles;
