import CastStyles from "./CastStyles.js";
import Thumbnail from "../Thumbnail/index.jsx";

const Cast = ({ cast }) => {
  const renderCast = () => {
    return cast.map((castItem, index) => {
      const { image, name } = castItem.person;

      return (
        <li key={index}>
          <Thumbnail
            imageUrl={(image && image.medium) || undefined}
            caption={name}
            small
          />
        </li>
      );
    });
  };

  return (
    <div className="cast">
      <h2>Cast</h2>
      <ul className="cast__list">{renderCast()}</ul>
      <style jsx>{CastStyles}</style>
    </div>
  );
};

export default Cast;
