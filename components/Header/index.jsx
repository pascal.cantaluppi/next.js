// import React from "react";
// import { useState } from "react";
// import { useRouter } from "next/router";

// const countries = [
//   { value: "us", label: "United States" },
//   { value: "gb", label: "Great Britain" },
// ];

// const Header = () => {
//   const router = useRouter();
//   const [selectedCountry, setSelectedCountry] = useState(router.query.country);

//   const handleChange = (event) => {
//     setSelectedCountry(event.target.value);
//     router.push(`/[country]`, `/${event.target.value}`);
//   };

//   const renderCountries = () => {
//     return countries.map((country) => {
//       return (
//         <option key={country.value} value={country.value}>
//           {country.label}
//         </option>
//       );
//     });
//   };

//   return (
//     <div className="header">
//       <select onChange={handleChange} value={selectedCountry}>
//         {renderCountries()}
//       </select>
//       {/* <style jsx>{HeaderStyles}</style> */}
//     </div>
//   );
// };

// export default Header;

import React, { useState, useEffect } from "react";
import cookies from "nookies";

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from "reactstrap";

import { useRouter } from "next/router";

const countries = [
  { value: "us", label: "United States" },
  { value: "gb", label: "Great Britain" },
];

const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  const router = useRouter();
  const [selectedCountry, setSelectedCountry] = useState(router.query.country);

  const handleChange = (event) => {
    setSelectedCountry(event.target.value);
    router.push(`/[country]`, `/${event.target.value}`);
  };

  const renderCountries = () => {
    return countries.map((country) => {
      return (
        <option key={country.value} value={country.value}>
          {country.label}
        </option>
        // <DropdownItem key={country.value} value={country.value}>
        //   {country.label}
        // </DropdownItem>
      );
    });
  };

  useEffect(() => {
    cookies.set(null, "defaultCountry", selectedCountry, {
      maxAge: 30 * 24 * 60 * 60,
      path: "/",
    });
  }, [selectedCountry]);

  return (
    <div>
      <Navbar color="dark" light expand="md">
        <NavbarBrand href="/" className="text-white">
          TV Show Broadcast | A Next.js App
        </NavbarBrand>
        {/* <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret className="text-white">
                Select Region
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>United States</DropdownItem>
                <DropdownItem>Great Britain</DropdownItem>
                {renderCountries()}
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse> */}
        <select onChange={handleChange} value={selectedCountry}>
          {renderCountries()}
        </select>
        {/* <NavbarText className="text-white">
          A Next.js server-side rendered React App
        </NavbarText> */}
      </Navbar>
    </div>
  );
};

export default Header;
