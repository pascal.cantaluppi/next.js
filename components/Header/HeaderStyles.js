import css from "styled-jsx/css";

const HeaderStyles = css`
  .header {
    text-align: center;
  }
`;

export default HeaderStyles;
