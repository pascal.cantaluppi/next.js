import css from "styled-jsx/css";

const DetailStyles = css`
  .details__container {
    width: 50%;
    max-width: 800px;
    border: 0px;
    height: auto;
    padding: 6px;
  }
  .details__poster {
    height: 600px;
    width: 400px;
    background-size: cover;
  }
`;

export default DetailStyles;
