import css from "styled-jsx/css";

// https://fonts.google.com/specimen/Raleway?query=raleway

const AppStyles = css`

@font-face {
  font-family: 'raleway';
  src: url('/fonts/raleway/Raleway-Medium.ttf') format('truetype');
}

:global(html) {
  font-family: 'raleway';
}

:global(ul) {
  padding: 0:
  margin: 0;
  list-style-type: none;
}

`;

export default AppStyles;
